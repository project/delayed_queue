<?php

function delayed_queue_settings_form() {
  $form = array();

  $variable_name = 'delayed_queue_verbose';
  $form['delayed_queue_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose'),
    '#description' => t('Add verbosity at queue processing for Delayed Queues.'),
    '#default_value' => variable_get($variable_name, FALSE),
  );

  $form['queues'] = array(
    '#type' => 'fieldset',
    '#title' => t('Queue delays'),
  );

  $queues = delayed_queues_defined_queues();
  foreach($queues as $key => $item) {
    $variable_name = 'delayed_queue_' . $key . '_dequeue_delay';
    $form['queues'][$variable_name] = array(
      '#type' => 'textfield',
      '#title' => $key,
      '#default_value' => variable_get($variable_name),
      '#size' => 7,
      '#maxlength' => 9,
      '#field_suffix' => t('secs.'),
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }

  return system_settings_form($form);
}

/**
 * Get queues defined with hook_queue_info().
 *
 * @return Array of TaggedSystemQueue queues indexed by name.
 */
function delayed_queues_defined_queues() {
  // Invoke hook_queue_info().
  $queues = module_invoke_all('cron_queue_info');
  foreach($queues as $key => $item) {
    if (!(DrupalQueue::get($key) instanceof TaggedSystemQueue)) {
      unset($queues[$key]);
    }
  }
  return $queues;
}
